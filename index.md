# P2P Web
## Self Sovereign Tech Meetup
## Christophe Benz

---

P2P web ?

decentralisé ?

ça vous évoque quoi ?

---

moi

étudiant permanent

---

principes

---

![](https://openclipart.org/download/277506/Centralized-Decentralized-and-Distributed-Networks-2017041028.svg)

---

vocabulaire

- P2P
- décentralisé
- distribué
- fédéré

---

- no backend
- offline first
- plusieurs peers peuvent servir le même contenu
- peer : client+serveur

---

exemples de plateformes centralisées

---

Twitter / Medium / WordPress / Jekyll / YouTube / SoundCloud / Flickr / Reddit / Hacker News / Discourse / Google Drive / Dropbox / GitHub / GitLab / npm / Wikipedia / Internet Archive / Matrix / Rocket Chat / Mastodon / Peertube

---

problèmes
dangers
web actuel

---

trinet
neutralité

---

contrôle
grands groupes
vs
utilisateurs

---

disponibilité = €€€

---

populations déconnectées
pays pauvres

---

données personnelles
éparpillées

---

identitées multipliées
login/password

---

interopérabilité ?

WhatsApp, Telegram, Rocket Chat, Mattermost, Slack, Gitter, Facebook Messenger, Twitter, Mastodon, Matrix (with Riot), Hangouts, Skype, Discourse, Email, SMS

---

offline

Twitter dans le métro

---

Sauvons le Titanic ?

Construisons un canot !

---

Cas d'usage

Publier une page web

---

auto-hébergement ?

---

GitHub Pages ?

---

domaine, SSL ...

---

Beaker browser !

---

le browser est le serveur !

---

aucun upload

---

URL

dat://0123456789.....

dérivée du contenu

---

les autres Beaker accèdent au contenu et forment un réseau P2P

---

Démo

---

![](https://staltz.com/beaker-frontend-dev-dream-browser/seeding.png)

---

avantages P2P

---

disponibilité = nombre de peers qui partagent

---

vol de bande passante révolu

---

données immutables

URLs versionnées

dat://0123...+{version}

---

fork

---

existing communities / technologies

---

Blockchain-based variants

Resource-based variants

Social-based variants

---

Secure Scuttlebutt

---

Demo

Patchwork

---

grandes questions

---

protection des lecteurs

Wikipedia on Dat

---

contenus illégaux

---

GDPR, droit à l'oubli

---

droit d'interprétation / droit à l'expression

---

questions ? idées ?

---

how to code, c'est l'atelier !
    - ressources
        - https://github.com/mixmix/flume-intro
        - https://github.com/ssbc/scuttlebutt-guide
